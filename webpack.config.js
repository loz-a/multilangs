const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const ENV_DEVELOPMENT = 'development'
const ENV_PRODUCTION  = 'production'

const NODE_ENV = process.env.NODE_ENV || ENV_DEVELOPMENT

module.exports = {
    devtool: NODE_ENV ? 'cheap-inline-module-source-map' : null,

    entry: {
        index: './src/index.js'
    },

    output: {
        path: path.join(__dirname, '..'),
        filename: 'multilangs.js'
    },

    watch: NODE_ENV == ENV_DEVELOPMENT,

    watchOptions: {
      aggregateTimeout: 100
    },

    module: {
        rules: [
            {
                test: /\.js$/,  
                exclude: /(node_modules|bower_components)/,
                use: [
                    { 
                        loader: 'babel-loader',
                        options: {
                            plugins: ['transform-runtime', 'transform-decorators-legacy'],
                            presets: [
                                ['es2015', {'modules': false}],
                                "stage-0", 
                                "react"
                            ]
                        } 
                    }
                ],                
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader'
                        }
                    ]
                })
            }, 
            {
                test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,                
                loader: 'url-loader',
                options: {
                    name: '[path][name].[ext]',
                    limit: '4096'
                }
            }
        ]
    },

    plugins: [
        new ExtractTextPlugin('../css/multilangs.css'),        
        new webpack.DefinePlugin({
          DEVELOPMENT: JSON.stringify(NODE_ENV === ENV_DEVELOPMENT),
          PRODUCTION: JSON.stringify(NODE_ENV !== ENV_DEVELOPMENT),
          'process.env': {
            NODE_ENV: JSON.stringify(NODE_ENV)
          }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    ],

    resolve: {
        modules: [
            path.join(__dirname, 'src'),
            'node_modules'
        ],        
        alias: {
            'app': path.resolve(__dirname, './src')
        }
    }
};


if (NODE_ENV === ENV_PRODUCTION) {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    )

    // module.exports.module.loaders[0].query.presets.push("react-hmre")
}
