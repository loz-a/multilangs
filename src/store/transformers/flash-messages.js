import { createTransformer } from 'mobx'

export default createTransformer(
  (store) => {
    const fm = store.flashMessages
    return fm.keys().map((id) => fm.get(id))
  }
)
