import { createTransformer } from 'mobx'
import { mapObjectFromMap, mapObjectsFromMap } from '../utils'

export default createTransformer((store) => {
  const languages = store.languages
  const mapLocale = (entity) => mapObjectFromMap(entity, 'locale', store.locales)
  const mapDisplayValues = (entity) => mapObjectsFromMap(
    entity,
    'displayValues',
    store.displayValues,
    (entity) => {
      return { ...entity, locale: mapLocale(entity)}
    }
  )

  return languages.keys().reduce((acc, id) => {    
    const entity = languages.get(id)
    acc[id] = {
      ...entity,
      locale: mapLocale(entity),
      displayValues: mapDisplayValues(entity)
    }
    return acc
  }, {})
})
