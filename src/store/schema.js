import { schema } from 'normalizr'

const locale = new schema.Entity('locales')
const displayValue = new schema.Entity('displayValues', { locale }, {
  processStrategy: (entity) => {
    entity.displayValue = entity.display_value
    delete entity.display_value
    return entity
  }
})

const language = new schema.Entity('languages',
  {
    locale,
    displayValues: new schema.Array(displayValue)
  },
  {
    processStrategy: (entity) => {
      entity.displayValues = entity.display_values
      entity.enabled = entity.is_enable
      delete entity.display_values
      delete entity.is_enable
      return entity
    }
  }
)

export default {
  languages: new schema.Array(language)
}
