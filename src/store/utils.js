// export function mapObject(entity, propName, store) {
//   if (undefined === entity[propName]) return null
//   return store[entity[propName]]
// }
//
// export function mapObjects(entity, propName, store) {
//   if (undefined === entity[propName]) return null
//   return entity[propName].reduce((acc, id) => {
//     acc[id] = store[id]
//     return acc
//   }, {})
// }

export function mapObjectFromMap(entity, propName, storeAsMap, cb = null) {
  if (undefined === entity[propName]) return null
  const item = storeAsMap.get(entity[propName])
  return typeof cb === 'function' ? cb(item, storeAsMap) : item
}

export function mapObjectsFromMap(entity, propName, storeAsMap, cb = null) {
  if (undefined === entity[propName]) return null
  return entity[propName].reduce((acc, id) => {
    const item = storeAsMap.get(id)
    acc[id] = typeof cb === 'function' ? cb(item, storeAsMap) : item
    return acc
  }, {})
}
