import { extendObservable, observable, action } from 'mobx'
import { normalize } from 'normalizr'
import initStateSchema from './schema'
import shortid from 'shortid'

import transformLocales from './transformers/locales'
import transformDisplayValues from './transformers/display-values'
import transformLanguages from './transformers/languages'
import transformFlashMessages from './transformers/flash-messages'

export default function(INIT_STATE = {}) {
  const initState = normalize(INIT_STATE, initStateSchema).entities

  const store = {
    supportedLocales: {
      origin: INIT_STATE.supportedLocales
    }
  }

  extendObservable(store, {
    languages: observable.map(initState.languages),
    locales: observable.map(initState.locales),
    displayValues: observable.map(initState.displayValues),
    flashMessages: observable.shallowMap({})
  })

  this.languages = {
    all: () => transformLanguages(store),

    byId: (id) => {
      const languages = this.languages.all()
      if (!languages[id]) throw new Error(`Language with id: ${id} is not exists in store`)
      return languages[id]
    },

    has: (language) => {
      const filtered = Object
        .values(this.languages.all())
        .filter((item) => item.language === language)

      return filtered.length > 0
    },

    save: action((data) => {
      const language = { ...data }

      if (undefined !== data['locale']) {
        const locale = data['locale']
        store.locales.set(locale.id, locale)
        language['locale'] = locale.id
      }

      if (undefined !== data['displayValues']) {
        const dvs = data['displayValues']
        const dvsIds = []
        Object.values(dvs).forEach((item) => {
          store.displayValues.set(item.id, { ...item, locale: item.locale.id})
          dvsIds.push(item.id)
        })
        language['displayValues'] = dvsIds
      }

      store.languages.set(data.id, language)
    }),

    delete: (language) => {
      store.locales.delete(language.locale.id),
      Object.keys(language.displayValues).forEach(action((id) => store.displayValues.delete(id)))
      store.languages.delete(language.id)
    },

    size: () => Object.keys(this.languages.all()).length
  }

  this.supportedLocales = {
    all: () => {
      if (undefined === store.supportedLocales.all) {
        const { origin } = store.supportedLocales
        const normalized = Object.keys(origin).reduce((acc, locale) => {
          const [lang] = locale.split('_')
          if (lang in acc) acc[lang].locales[locale] = origin[locale]
          else acc[lang] = {
            id: lang,
            language: origin[locale].split(' ')[0],
            locales: { [locale]: origin[locale] }
          }
          return acc
        }, {})
        store.supportedLocales.all = Object.freeze(normalized)
      }
      return store.supportedLocales.all
    },
    byLanguage: (language) => this.supportedLocales.all()[language],
    byLocale: (locale) => store.supportedLocales.origin[locale]
  }

  this.locales = {
    all: () => transformLocales(store),
    byName: (name) => Object.values(this.locales.all()).find((locale) => locale.locale === name),
    size: () => Object.keys(this.locales.all()).length
  }

  this.displayValues = {
    all: () => transformDisplayValues(store),
    byId: (id) => {
      const dvs = this.displayValues.all()
      if (!dvs[id]) throw new Error(`DisplayValue with id: ${id} is not exists in store`)
      return dvs[id]
    }
  }

  this.flashMessages = {
    messages: () => transformFlashMessages(store),
    clear: () => store.flashMessages.clear(),
    add: (type, text, header = null) => {
      const id = shortid.generate()
      store.flashMessages.set(id, {id, type, text, header })
    },
    delete: (id) => {
      if (store.flashMessages.has(id)) store.flashMessages.delete(id)
    },
    warning: (msg, header = null) => this.flashMessages.add('warning', msg, header),
    error: (msg, header = null) => this.flashMessages.add('error', msg, header),
    success: (msg, header = null) => this.flashMessages.add('success', msg, header),
    info: (msg, header = null) => this.flashMessages.add('info', msg, header)
  }

  // this.observableStore = store

}
