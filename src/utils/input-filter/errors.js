import { isObject } from 'app/utils'

const normalizeMessages = (messages) => {
  if (Array.isArray(messages)) return messages

  if (isObject(messages)) {
    const result = {}
    for (let input in messages) if (messages.hasOwnProperty(input)){
      result[input] = normalizeMessages(messages[input])
    }
    return result
  }

  return [ messages + '' ]
}

const everyFirstMessage = (errors) => {
  return Object.keys(errors).reduce((acc, input) => {
    if (Array.isArray(errors[input])) acc[input] = errors[input][0]
    else if (isObject(errors[input])) acc[input] = everyFirstMessage(errors[input])
    return acc
  }, {})
}


function Errors() {
  const errors = {}

  this.populate = (data) => {
    if (!isObject(data)) throw new Error('Expected input parameter type must be Object')

    for (let input in data) if (data.hasOwnProperty(input)) {
      errors[input] = normalizeMessages(data[input])
    }

    return this
  }

  this.has = (input) => {
    return errors.hasOwnProperty(input)
  }

  this.set = (input, messages) => {
    errors[input] = normalizeMessages(messages)
  }

  this.get = {
    byName: (input) => this.has(input) ? errors[input] : null,

    all: () => errors,

    asArray: () => Object.values(errors).reduce((acc, messages) => {
      acc.push(...messages)
      return acc
    }, []),

    everyFirstMessage: () => everyFirstMessage(errors)
  }

  this.clear = (input = null) => {
    if (input) {
      if (this.has(input)) delete errors[input]
    } else {
      error = {}
    }
    return this
  }

  this.size = () => Object.keys(errors).length
}

export default Errors
