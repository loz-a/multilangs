import { isObject, hasCallableProperty, hasPropertyInstanceOf } from 'app/utils'
import Errors from './errors'

function isUndefined(value) {
  return value === undefined
}

function isEmptyString(value) {
  return typeof value === 'string' && value.length == 0
}

function InputRequiredValidator(validators) {
  const requiredInputs = validators.hasOwnProperty('__required') ? validators['__required'] : {}
  if (!isObject(requiredInputs)) throw new Error('Expected input parameter type must be Object (in InputFilter.isInputRequired)')

  this.isRequired = (input) => {
    return requiredInputs.hasOwnProperty(input) && requiredInputs[input] === true
  }

  this.validate = (obj, input) => {
    const isEmpty = !obj.hasOwnProperty(input) || isUndefined(obj[input]) || isEmptyString(obj[input])
    if (isEmpty) {
      return "Значення є обов'язковим для заповнення"
    }
    return true
  }
}

function InputFilter(validators = null, filters = null, postProcessor = null) {

  this.validate = function(data = {}, dependencies = null) {
    if (!isObject(data)) throw new Error('Expected input parameter type must be Object (in InputFilter.validate)')

    const accObj = {data: {}, errors: {}}
    const requiredValidator = new InputRequiredValidator(validators)

    let validateResult = Object.keys(data).reduce((acc, input) => {

      // validate required inputs
      if (requiredValidator.isRequired(input)) {
        const currResult = requiredValidator.validate(data, input)
        if (currResult !== true) {
          acc.errors[input] = [currResult]
          return acc
        }
      }

      if (hasPropertyInstanceOf(validators, input, InputFilter)) {
        // delegate validation to inner validator
        const currResult = validators[input].validate(data[input], dependencies)
        if (currResult.hasOwnProperty('errors')) acc.errors[input] = currResult['errors'].get.all()
        else acc.data[input] = currResult['data']

      } else if (hasCallableProperty(validators, input)) {
        // filter data
        const inputData = (hasCallableProperty(filters, input)) ? filters[input](data[input]) : data[input]
        // validate data
        const result = validators[input](inputData, data, dependencies)

        if (result === true) acc.data[input] = inputData
        else acc.errors[input] = result

      } else {
        // only filter data if filter function exists, else set raw data
        acc.data[input] = hasCallableProperty(filters, input) ? filters[input](data[input]) : data[input]
      }

      return acc
    }, accObj)

    if (!Object.keys(validateResult.errors).length) return {
      data: typeof postProcessor === 'function' ? postProcessor(validateResult.data) : validateResult.data
    }

    return {
      data,
      errors: (new Errors()).populate(validateResult.errors)
    }
  }

  this.asyncValidate = function(data, dependencies = null) {
    return new Promise(
      (resolve, reject) => {
        const result = this.validate(data, dependencies)
        return result.hasOwnProperty('errors') ? reject(result) : resolve(result)
      }
    )
  }

}

export default InputFilter
