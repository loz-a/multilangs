import { PropTypes } from 'react'

export const LocalePropType = PropTypes.shape({
  id: PropTypes.string,
  locale: PropTypes.string.isRequired
})

export const DisplayValuePropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  displayValue: PropTypes.string.isRequired,
  locale: LocalePropType.isRequired
})

export const LanguagePropType = PropTypes.shape({
  id: PropTypes.string,
  language: PropTypes.string.isRequired,
  locale: LocalePropType.isRequired,
  description: PropTypes.string,
  enabled: PropTypes.string.isRequired,
  displayValues: PropTypes.objectOf(DisplayValuePropType)
})
