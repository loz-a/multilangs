export const isObject = (val) => {
  return val != null && typeof val === 'object' && Array.isArray(val) === false;
}

export const hasCallableProperty = (obj, property) => {
  if (isObject(obj)) {
    return obj.hasOwnProperty(property) && typeof obj[property] === 'function'
  }
  return false
}

export const hasPropertyInstanceOf = (obj, property, Instance) => {
  if (isObject(obj)) {
    return obj.hasOwnProperty(property) && obj[property] instanceof Instance
  }
  return false
}

export const isElement = (element, type) =>
  element && element.type && element.type === type

export const isElementOneOf = (element, types) => {
  if (Array.isArray(types)) {
      for (let i = 0; i < types.length; i++) {
        if (isElement(element, types[i])) return true
      }
  } else {
    return isElement(element, types)
  }
  return false
}

// inspired by https://atticuswhite.com/blog/render-if-conditionally-render-react-components/
export const ifExpr = (predicate) => (expr, elseExpr = null) =>
  predicate
    ? ( typeof expr === 'function' ? (expr.bind(this))() : expr )
    : ( typeof elseExpr === 'function' ? (elseExpr.bind(this))() : elseExpr)
