import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const clickHandler = (evt) => evt.preventDefault()

const Tab = ({
  href,
  text,
  icon
}) => {

  if (location.pathname === href) return (
    <li className="active">
      <a href="#" data-toggle="tab" onClick={clickHandler}>
        <i className={icon}></i> {text}
      </a>
    </li>
  )

  return (
    <li>
      <Link to={href} data-toggle="tab">
        <i className={icon}></i> {text}
      </Link>
    </li>
  )
}

Tab.propTypes = {
  href: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired
}

export default Tab
