import React, { PropTypes, cloneElement, Children } from 'react'
import Tab from './tab'

const filterChildren = (children, condition) => {
  if (!children) return []
  return Children.map(children, (child, idx) => {
    if (child && child.type && condition(child)) return cloneElement(child, {key: idx})
  })
}

const Tabs = ({
  children
}) => {
  const tabs = filterChildren(children, (child) => child.type === Tab)
  const content = filterChildren(children, (child) => child.type !== Tab)

  return (
    <div className="tabbable tabs-left">

      <ul className="nav nav-tabs">
        {tabs}
      </ul>

      <div className="tab-content">
        {content.length > 1
          ? <div>{content}</div>
          : content
        }
      </div>
    </div>
  )
}

Tabs.propTypes = {
  children: PropTypes.node
}

Tabs.Tab = Tab

export default Tabs
