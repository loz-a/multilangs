import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const No = ({
  value = 'Hi',
  href,
  onClick,
  ...props
}) => (
  <Link onClick={onClick} to={href} {...props} >
    <i className="zmdi zmdi-undo"></i> {value}
  </Link>
)

No.propTypes = {
  href: PropTypes.string.isRequired,
  value: PropTypes.string,
  onClick: PropTypes.func
}

export default No
