import React, { PropTypes } from 'react'

const Yes = ({
  href='',
  value = 'Так',
  onClick,
  ...props
}) => (
  <a onClick={onClick} href={href} {...props} >
    <i className="zmdi zmdi-close"></i> {value}
  </a>
)

Yes.propTypes = {
  value: PropTypes.string,
  href: PropTypes.string,
  onClick: PropTypes.func.isRequired
}

export default Yes
