import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Submit = ({
  href,
  value,
  onClick,
  ...props
}) => (
  <Link onClick={onClick} to={href} {...props} >
    <i className="zmdi zmdi-check"></i> {value}
  </Link>
)

Submit.propTypes = {
  href: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

export default Submit
