import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Delete = ({
  href,
  value,
  onClick,
  ...props
}) => (
  <Link onClick={onClick} to={href} {...props} >
    <i className="zmdi zmdi-delete"></i> {value}
  </Link>
)

Delete.propTypes = {
  href: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

export default Delete
