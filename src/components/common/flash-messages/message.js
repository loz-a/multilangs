import React, { PropTypes } from 'react'

class Message extends React.PureComponent {

  componentDidMount() {
    this.timer = setTimeout(
      this.props.onClose,
      this.props.timeout
    )
  }

  componentWillUnmount() {
    clearTimeout(this.timer)
  }

  render() {
    const { header, type, text } = this.props.message
    const alertClass = `alert alert-block ${type !== 'warning' ? `alert-${type}` : ''} app-alert`

    return (
      <div className={alertClass}>
        {header && <h4>{header}</h4>}
        {text}
      </div>
    )
  }

  static propTypes = {
    message: PropTypes.shape({
      header: PropTypes.string,
      type: PropTypes.oneOf(['error', 'success', 'warning', 'info']).isRequired,
      text: PropTypes.string.isRequired,
    }),
    onClose: PropTypes.func,
    timeout: PropTypes.number
  }

  static defaultProps = {
    timeout: 3000
  }
}

export default Message
