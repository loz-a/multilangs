import React, { PropTypes } from 'react'
import { computed } from 'mobx'
import { inject, observer } from 'mobx-react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Message from './message'
import './styles.css'

@inject('store')
@observer
class FlashMessages extends React.PureComponent {

  @computed get flashMessages() {
    return this.props.store.flashMessages
  }

  deleteMessage = (messageId) => () => {
    this.flashMessages.delete(messageId)
  }

  render() {
    const messages = this.flashMessages.messages()

    if (!messages.length) return null

    return (
      <ReactCSSTransitionGroup
        component="div"
        transitionName="animated"
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={100}
      >
        {messages.map((message) => {
          return <Message
            key={message.id}
            onClose={this.deleteMessage(message.id)}
            message = {message} />
        })}
      </ReactCSSTransitionGroup>
    )
  }

  static propTypes = {
    store: PropTypes.object.isRequired
  }
}

export default FlashMessages
