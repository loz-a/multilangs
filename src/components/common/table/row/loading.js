import React, { PropTypes } from 'react'
import cx from 'classnames'

const RowLoading = ({
  colSpan,
  text = "Loading..."
}) => (
  <tr className="warning">
    <td colSpan={colSpan}>{text}</td>
  </tr>
)

RowLoading.propTypes = {
  colSpan: PropTypes.number.isRequired,
  text: PropTypes.string
}

export default RowLoading
