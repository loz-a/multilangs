import React from 'react'
import BaseSelect from 'react-select';
import 'react-select/dist/react-select.css';
import './styles.css'

const Select = ({
  name,
  value,
  options,
  onChange,
  hideArrow = false,
  ...otherProps
}) => (
  <BaseSelect
    className="overlap-overflow"
    arrowRenderer={hideArrow ? () => '' : undefined}
    { ...otherProps }
    name={name}
    value={value}
    options={options}
    onChange={onChange}
  />
)

export default Select
