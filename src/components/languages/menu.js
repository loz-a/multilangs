import React from 'react'
import { Link } from 'react-router'
import { urls } from 'app/routes/goto'

const Menu = () => (
  <ul className="nav nav-pills">
    <li>
      <Link to={urls.languageSave()}>
        <i className="zmdi zmdi-playlist-plus zmdi-hc-lg"></i> Додати мову
      </Link>
    </li>
  </ul>
)

export default Menu
