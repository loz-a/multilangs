import React, { PropTypes } from 'react'
import { computed } from 'mobx'
import { urls } from 'app/routes/goto'
import Edit from 'app/components/common/form/link/edit'
import Delete from 'app/components/common/form/link/delete'

class Item extends React.PureComponent {

  @computed get isLoading() {
    return this.props.isLoading
  }

  @computed get language() {
    return this.props.language
  }

  @computed get flagIcon() {
    return `${this.language.locale.locale.slice(-2).toLowerCase()} flag`
  }

  render() {
    const language = this.language
    return (
      <tr>
        <td className="flag-icon-box">
          <i className={this.flagIcon}></i>
        </td>
        <td>{language.language}</td>
        <td>{language.locale.locale}</td>
        <td>{language.description}</td>
        <td>{language.enabled === '1' ? 'Вкл.' : 'Відкл.'}</td>
        <td>
          {!this.isLoading &&
            <Edit href={urls.languageSave(language.id)} value="Ред." title="Редагувати" />
          }
          {!this.isLoading &&
            <Delete href={urls.languageDelete(language.id)} value="Видал." title="Видалити"/>
          }
        </td>
      </tr>
    )
  }

  static propTypes = {
    language: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired
  }

}

export default Item
