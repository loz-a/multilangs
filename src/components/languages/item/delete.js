import React, { PropTypes } from 'react'
import { observable, action, computed } from 'mobx'
import { observer, inject } from 'mobx-react'
import { Link } from 'react-router'
import { LanguagePropType } from 'app/utils/propTypes'
import goto, { urls } from 'app/routes/goto'
import api from 'app/api'
import Yes from 'app/components/common/form/link/yes'
import No from 'app/components/common/form/link/no'

@inject('store')
@observer
class Delete extends React.PureComponent {

  @computed get language() {
    return this.props.language
  }

  @computed get flagIcon() {
    return `${this.language.locale.locale.slice(-2).toLowerCase()} flag`
  }

  @action onSubmit = (evt) => {
    evt.preventDefault()

    const { store, enableLoader } = this.props
    const { flashMessages } = store
    const language = this.language

    enableLoader()

    api
      .deleteLangauge(language.id)
      .then(action(() => {
        store.languages.delete(language)
        flashMessages.success('Мову успішно видалено', 'Успіх.')
        enableLoader(false)
        goto(urls.languages())
      }))
  }

  render() {
    const { isLoading } = this.props
    const language = this.language

    return (
      <tr className="error">
        <td className="flag-icon-box">
          <i className={this.flagIcon}></i>
        </td>
        <td colSpan="4">
          {isLoading
            ? this.renderProgressBar()
            : `Ви дійсно хочете видалити мову ${language.language} та локаль ${language.locale.locale}?`
          }
        </td>

        <td>
            <Yes onClick={this.onSubmit} title="Так" className="danger-btn" />
            <No href={urls.languages()} title="Ні" />
        </td>

      </tr>
    )
  }

  renderProgressBar() {
    return (
      <div className="progress progress-danger active">
        <div className="bar"></div>
      </div>
    )
  }

  static propTypes = {
    store: PropTypes.object.isRequired,
    language: PropTypes.object.isRequired,
    enableLoader: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired
  }
}

export default Delete
