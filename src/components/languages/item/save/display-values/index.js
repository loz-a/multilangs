import React, { PropTypes, cloneElement } from 'react'
import { computed } from 'mobx'
import { inject, observer } from 'mobx-react'
import shortid from 'shortid'
import { isElementOneOf, ifExpr } from 'app/utils'
import { LanguagePropType } from 'app/utils/propTypes'
import Menu from './menu'
import Add from './item/add'
import Edit from './item/edit'
import Delete from './item/delete'
import Item from './item'
import './styles.css'

@inject('store')
@observer
class DisplayValues extends React.PureComponent {

  @computed get language() {
    return this.props.language
  }

  get paramDisplayValueId() {
    return this.props.params.displayValueId
  }

  @computed get isDisplayValueAddDisabled() {
    return (
      Object.keys(this.props.language.displayValues).length >= this.props.store.languages.size()
    )
  }

  render() {
    return (
      <div>
        <Menu isDisplayValueAddDisabled={this.isDisplayValueAddDisabled} />
        <table className="table table-hover table-bordered display-values-list">
          {this.renderTHead()}
          {this.renderTBody()}
        </table>
      </div>
    )
  }

  renderTHead = () => (
    <thead>
      <tr>
        <th className="flag-icon-box">
          <i className="zmdi zmdi-flag"></i>
        </th>
        <th>Локаль</th>
        <th>Вивід локалі</th>
        <th>Дії</th>
      </tr>
    </thead>
  )

  renderTBody = () => {
    const { children } = this.props
    const language = this.language
    const paramDisplayValueId = this.paramDisplayValueId

    const ifChildrenIsAdd = ifExpr(isElementOneOf(children, Add)).bind(this)
    const ifChildrenIsDeleteOrEdit = ifExpr(isElementOneOf(children, [Delete, Edit])).bind(this)

    return (
      <tbody>
        {(Object.entries(language.displayValues).map(([id, displayValue]) => {
          return (paramDisplayValueId === id)
            ? ifChildrenIsDeleteOrEdit(this.renderChildren({
                key: id,
                language
              }))
            : <Item key={displayValue.id} displayValue={displayValue} />
        }))}

        {ifChildrenIsAdd(
          this.renderChildren({ language })
        )}
      </tbody>
    )
  }

  renderChildren = (props = {}) => () => cloneElement(this.props.children, props)

  static propTypes = {
    language: LanguagePropType,
    store: PropTypes.object.isRequired
  }
}

DisplayValues.Add = Add
DisplayValues.Edit = Edit
DisplayValues.Delete = Delete

export default DisplayValues
