import React, { PropTypes } from 'react'
import { Link, withRouter } from 'react-router'
import { urls } from 'app/routes/goto'

const Menu = ({
  router,
  isDisplayValueAddDisabled
}) => {
  if (isDisplayValueAddDisabled) return null

  return (
    <ul className="nav nav-pills">
        <li>
          <Link to={urls.displayValueAdd(router.params.languageId)}>
            <i className="zmdi zmdi-playlist-plus zmdi-hc-lg"></i> Додати вивід локалі
          </Link>
        </li>
    </ul>
  )
}


Menu.propTypes = {
  isDisplayValueAddDisabled: PropTypes.bool.isRequired
}

export default withRouter(Menu)
