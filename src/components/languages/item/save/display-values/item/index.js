import React, { PropTypes } from 'react'
import { observable, computed } from 'mobx'
import { observer } from 'mobx-react'
import { Link, withRouter } from 'react-router'
import { DisplayValuePropType } from 'app/utils/propTypes'
import { urls } from 'app/routes/goto'

import Edit from 'app/components/common/form/link/edit'
import Delete from 'app/components/common/form/link/delete'
import RowLoading from 'app/components/common/table/row/loading'

@withRouter
@observer
class Item extends React.PureComponent {
  @observable isLoading = false

  @computed get displayValue() {
    return this.props.displayValue
  }

  @computed get paramLanguageId() {
    return this.props.params.languageId
  }

  @computed get flagIcon() {
    return `${this.displayValue.locale.locale.slice(-2).toLowerCase()} flag`
  }

  render() {
    const displayValue = this.displayValue

    if (this.isLoading) return (<RowLoading colSpan={4}/>)

    return (
      <tr>
        <td>
          <div className="ui center aligned">
            <i className={this.flagIcon}></i>
          </div>
        </td>
        <td>{displayValue.locale.locale}</td>
        <td>{displayValue.displayValue}</td>
        <td className="actions">
          <div className="ui horizontal list">
            <Edit href={urls.displayValueEdit(this.paramLanguageId, this.displayValue.id)} value="Редагувати"/>
            <Delete href={urls.displayValueDelete(this.paramLanguageId, this.displayValue.id)} value="Видалити"/>
          </div>
        </td>
      </tr>
    )
  }

  static propTypes = {
    displayValue: DisplayValuePropType.isRequired
  }
}


export default Item
