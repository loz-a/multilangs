import React, { PropTypes } from 'react'
import { computed, action } from 'mobx'
import { inject } from 'mobx-react'
import goto, { urls } from 'app/routes/goto'
import Yes from 'app/components/common/form/link/yes'
import No from 'app/components/common/form/link/no'

@inject('store')
class Delete extends React.PureComponent {

  @computed get language() {
    return this.props.language
  }

  @computed get flagIcon() {
    return `${this.language.displayValues[this.paramDisplayValueId].locale.locale.slice(-2).toLowerCase()} flag`
  }

  @computed get flashMessages() {
    return this.props.store.flashMessages
  }

  @computed get paramDisplayValueId() {
    return this.props.params.displayValueId
  }

  removeDisplayValue = action(() => {
    const dvs = this.language.displayValues
    const id = this.paramDisplayValueId

    this.language.displayValues = Object.keys(dvs)
      .filter((key) => id !== key)
      .reduce((acc, key) => {
        acc[key] = dvs[key]
        return acc
      }, {})
  })

  onSubmit = (evt) => {
    evt.preventDefault()
    this.removeDisplayValue()

    this.flashMessages.warning('Вивід мови видалено. Натисніть ОК, щоб зберегти дані', 'Увага.')
    goto(urls.displayValues(this.props.params.languageId))
  }

  render() {
    const displayValue = this.language.displayValues[this.paramDisplayValueId]

    return (
      <tr className="error">
        <td className="flag-icon-box">
          <i className={this.flagIcon}></i>
        </td>
        <td colSpan="2">
          Ви дійсно хочете видалити вивід локалі {displayValue.locale.locale}?
        </td>

        <td>
          <div className="ui horizontal list">
            <Yes  onClick={this.onSubmit} title="Так" className="danger-btn" />
            <No href={urls.displayValues(this.props.params.languageId)}  title="Ні" />
          </div>
        </td>

      </tr>
    )
  }

  static propTypes = {
    language: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
  }

}

export default Delete
