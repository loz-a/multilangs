import React, { PropTypes } from 'react'
import { observable, action, computed } from 'mobx'
import { observer, inject } from 'mobx-react'
import cx from 'classnames'
import shortid from 'shortid'
import goto, { urls } from 'app/routes/goto'
import Select from 'app/components/common/select'
import Submit from 'app/components/common/form/link/submit'
import Cancel from 'app/components/common/form/link/cancel'
import inputFilter from 'app/input-filter/display-value-add'

@inject('store')
@observer
class Add extends React.PureComponent {
  @observable locale = ''
  @observable displayValue = ''
  @observable flagIcon = ''
  @observable errors = observable.map({})

  @computed get enableLocales() {
    return this.props.store.locales.all()
  }

  @computed get language() {
    return this.props.language
  }

  @computed get localesOptions() {
    const addedLocales = Object.values(this.language.displayValues).map((item) => item.locale.locale)
    const enableLocales = this.enableLocales

    return Object
      .values(this.enableLocales)
      .map(({locale}) => {
        if (addedLocales.indexOf(locale) === -1) {
          const description = this.props.store.supportedLocales.byLocale(locale)
          return {
            value: locale,
            label: (
              <div>
                <i className={`${locale.slice(-2).toLowerCase()} flag`} />
                {`${locale} - ${description}`}
              </div>
            )
          }
        }
      })
      .filter((item) => item !== undefined)
  }

  @computed get flashMessages() {
    return this.props.store.flashMessages
  }

  @action addToLanguage = (locale, displayValue) => {
    const tempId = shortid.generate()
    this.language.displayValues[tempId] = {
      id: tempId + '',
      locale: this.props.store.locales.byName(locale),
      displayValue
    }
  }

  @action handleDisplayValueChange = (evt) => {
    this.displayValue = evt.target.value
    if (this.errors.has('displayValue')) this.errors.delete('displayValue')
  }

  @action handleLocalesChange = (locale) => {
    if (!locale) return
    this.locale = locale.value
    this.flagIcon = ''

    if (typeof this.locale === 'string' && this.locale.length) {
      this.flagIcon = `${this.locale.slice(-2).toLowerCase()} flag`
    }
    if (this.errors.has('locale')) this.errors.delete('locale')
  }

  @action handleSubmit = (evt) => {
    evt.preventDefault()

    const data = {
      locale: { locale: this.locale },
      displayValue: this.displayValue
    }

    inputFilter.asyncValidate(data, this.language).then(
      (result) => {
        const { data } = result
        this.addToLanguage(data.locale.locale, data.displayValue)
        this.flashMessages.warning('Вивід мови додано. Натисніть ОК, щоб зберегти дані', 'Увага.')
        goto(urls.displayValues(this.props.params.languageId))
      },
      (errors) => {
        this.errors.replace(errors.errors.get.everyFirstMessage())
        this.flashMessages.error('Деякі дані введені не вірно', 'Помилка.')
      }
    )

  }

  render() {
    const paramLanguageId = this.props.params.languageId

    return (
      <tr className={cx('display-values-list-item', {'error invalid-data': this.errors.size})}>

        <td className="flag-icon-box">
          <i className={this.flagIcon}></i>
        </td>

        <td>
          <Select
            id="input-language"
            value={this.locale}
            options={this.localesOptions}
            onChange={this.handleLocalesChange}
          />
          {this.errors.has('locale') &&
            <span className="help-inline">
              <i className="zmdi zmdi-alert-triangle"></i> {this.errors.get('locale').locale}
            </span>
          }
        </td>

        <td>
          <div className="ui input">
            <input type="text"
              value={this.displayValue}
              onChange={this.handleDisplayValueChange}
              placeholder="Новий вивід локалі"/>

            {this.errors.has('displayValue') &&
              <span className="help-inline">
                <i className="zmdi zmdi-alert-triangle"></i> {this.errors.get('displayValue')}
              </span>
            }
          </div>
        </td>

        <td className="actions">
          <div className="ui horizontal list">
            <Submit className="submit-btn"
              href={urls.displayValues(paramLanguageId)}
              onClick={this.handleSubmit}
              value="Ок" />

            <Cancel href={urls.displayValues(paramLanguageId)} value="Відмінити" />
          </div>
        </td>
      </tr>
    )
  }

  static propTypes = {
    language: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
  }

}

export default Add
