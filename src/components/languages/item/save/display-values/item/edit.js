import React, { PropTypes } from 'react'
import { action, computed, observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import cx from 'classnames'
import goto, { urls } from 'app/routes/goto'
import { DisplayValuePropType } from 'app/utils/propTypes'
import Add from './add'
import Submit from 'app/components/common/form/link/submit'
import Cancel from 'app/components/common/form/link/cancel'
import inputFilter from 'app/input-filter/display-value-edit'

@inject('store')
@observer
class Edit extends React.PureComponent {
  @observable newDisplayValue;
  @observable errors = observable.map({})

  constructor(props) {
    super(props)
    this.newDisplayValue = props.language.displayValues[this.paramDisplayValueId].displayValue
  }

  @computed get paramDisplayValueId() {
    return this.props.params.displayValueId
  }

  @computed get language() {
    return this.props.language
  }

  @computed get flagIcon() {
    return `${this.language.displayValues[this.paramDisplayValueId].locale.locale.slice(-2).toLowerCase()} flag`
  }

  @computed get flashMessages() {
    return this.props.store.flashMessages
  }

  @action handleDisplayValue = (evt) => {
    this.newDisplayValue = evt.target.value
    if (this.errors.has('displayValue')) this.errors.delete('displayValue')
  }

  @action handleSubmit = (evt) => {
    evt.preventDefault()

    const data = {
      id: this.paramDisplayValueId,
      displayValue: this.newDisplayValue
    }

    inputFilter.asyncValidate(data, this.language).then(
      action((result) => {
        const { data } = result
        this.language.displayValues[data.id].displayValue = data.displayValue

        this.flashMessages.warning('Вивід мови відредаговано. Натисніть ОК, щоб зберегти дані', 'Увага.')
        goto(urls.displayValues(this.props.params.languageId))
      }),
      (errors) => {
        this.errors.replace(errors.errors.get.everyFirstMessage())
        this.flashMessages.error('Деякі дані введені не вірно', 'Помилка.')
      }
    )
  }

  render() {
    const paramLanguageId = this.props.params.languageId
    const displayValue = this.language.displayValues[this.paramDisplayValueId]

    return (
      <tr className={cx({'error invalid-data': this.errors.size})}>
        <td className="flag-icon-box">
          <i className={this.flagIcon}></i>
        </td>
        <td>
          {displayValue.locale.locale}
        </td>

        <td>
          <div>
            <input type="text"
              defaultValue={displayValue.displayValue}
              onChange={this.handleDisplayValue}
              placeholder="Новий вивід локалі"/>

            {this.errors.has('displayValue') &&
              <span className="help-inline">
                <i className="zmdi zmdi-alert-triangle"></i> {this.errors.get('displayValue')}
              </span>
            }
          </div>
        </td>

        <td className="actions">
          <div className="ui horizontal list">
            <Submit className="submit-btn"
              href={urls.displayValues(paramLanguageId)}
              onClick={this.handleSubmit}
              value="Ок" />

            <Cancel href={urls.displayValues(paramLanguageId)} value="Відмінити" />
          </div>
        </td>
      </tr>
    )
  }

  static propTypes = {
    language: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
  }
}

export default Edit
