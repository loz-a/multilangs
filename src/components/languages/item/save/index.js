import React, { cloneElement, PropTypes } from 'react'
import { observable, toJS, action, computed } from 'mobx'
import { observer, inject } from 'mobx-react'
import goto, { urls } from 'app/routes/goto'
import api from 'app/api'
import inputFilter from 'app/input-filter/language'
import Tabs from 'app/components/common/tabs'
import withBreadcrumbs from 'app/decorators/breadcrumbs/with-breadcrumbs'
import './styles.css'

const normalizeLanguage = (data) => {
  return {
    id: data.id,
    language: data.language,
    locale: data.locale,
    enabled: data.is_enable,
    description: data.description,
    displayValues: (data.display_values || []).reduce((acc, item) => {
      acc[item.id] = {
        id: item.id,
        displayValue: item.display_value,
        locale: item.locale
      }
      return acc
    }, {})
  }
}


@withBreadcrumbs
@inject('store')
@observer
class SaveLanguage extends React.PureComponent {

  @observable isLoading = false;
  @observable errors = observable.map({})

  @computed get breadcrumbs() {
    return this.props.renderBreadcrumbs()
  }

  @computed get store() {
    return this.props.store.languages
  }

  @computed get flashMessages() {
    return this.props.store.flashMessages
  }

  @computed get language() {
    const { languageId } = this.props.params

    return (languageId)
      ? this.store.byId(languageId)
      : observable({
          language: '',
          locale: {
            locale: ''
          },
          enabled: '1',
          description: ''
        })
  }

  handleSubmit = action((evt) => {
    evt.preventDefault()
    this.isLoading = true

    const language = toJS(this.language)

    inputFilter.asyncValidate(language, this.props.store).then(
      (data) => {
        api.saveLanguage(data.data).then(action((result) => {
          this.isLoading = false
          this.store.save(normalizeLanguage(result.data))
          this.flashMessages.success(`Мову успішно ${(this.language.id) ? 'відредаговано': 'додано'}`, 'Успіх.')
          goto(urls.languageSave(result.data['id']))
        }))
      },
      action((errors) => {
        this.isLoading = false
        this.errors.replace(errors.errors.get.everyFirstMessage())
        this.flashMessages.error('Деякі дані введені не вірно', 'Помилка.')
      })
    )
  })

  routeToLangaugesList = () => goto(urls.languages())

  render() {
    return (
      <form
        className="form-horizontal language-form"
        onSubmit={this.handleSubmit}>

        {this.breadcrumbs}

        <Tabs>
          <Tabs.Tab href={urls.languageSave(this.language.id || null)} text='Деталі' icon='zmdi zmdi-collection-text' />
          {this.language.id
            && <Tabs.Tab href={urls.displayValues(this.language.id)} text='Вивід мови' icon='zmdi zmdi-eye'  />
          }

          {this.isLoading
            ? (<div className="loader-container">
                <i className="zmdi zmdi-refresh zmdi-hc-spin"></i> Перекуріть, дані оброблаяються...
              </div>)
            : cloneElement(this.props.children, {language: this.language, errors: this.errors})
          }
        </Tabs>

        <div className="form-submit">
          <div className="btn-group">
            <button type="submit" className="btn" disabled={this.isLoading}>
              <i className="zmdi zmdi-check zmdi-hc-lg"></i> Ok
            </button>
            <button type="button" className="btn" onClick={this.routeToLangaugesList} disabled={this.isLoading}>
              <i className="zmdi zmdi-undo zmdi-hc-lg"></i> Повернутись до списку мов
            </button>
          </div>
        </div>

      </form>
    )
  }

  static propTypes = {
    store: PropTypes.object.isRequired,
    renderBreadcrumbs: PropTypes.func.isRequired
  }
}

export default SaveLanguage
