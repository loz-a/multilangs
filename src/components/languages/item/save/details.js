import React, { PropTypes, Children, cloneElement } from 'react'
import { computed, action } from 'mobx'
import { inject, observer } from 'mobx-react'
import cx from 'classnames'
import { isObject } from 'app/utils'
import Select from 'app/components/common/select'

@inject('store')
@observer
class LanguageDetails extends React.PureComponent {

  get isEdit() {
    const { languageId } = this.props.params
    return languageId !== undefined
  }

  @computed get language() {
    return this.props.language
  }

  @computed get errors() {
    return this.props.errors
  }

  @computed get supportedLocales() {
    return this.props.store.supportedLocales.all()
  }

  @computed get languagesOptions() {
    return Object
      .values(this.supportedLocales)
      .map(({id, language}) => ({
        value: id,
        label: language
      }))
  }

  @computed get localesOptions() {
    if (!this.language.language) return []
    return Object.entries(this.supportedLocales[this.language.language].locales)
      .map(([locale, description]) => ({
          value: locale,
          label: (<div><i className={`${locale.slice(-2).toLowerCase()} flag`} />{`${locale} - ${description}`}</div>)
      }))
  }

  @action setLanguage = (language) => {
    if (this.isEdit || !isObject(language)) return
    this.language.language = language.value
    if (this.errors.has('language')) this.errors.delete('language')

    const languageLocales = this.supportedLocales[language.value].locales
    const localesIds = Object.keys(languageLocales)
    if (localesIds.length === 1) this.setLocale({value: localesIds[0]})
  }

  @action setLocale = (locale) => {
    if (this.isEdit || !isObject(locale)) return
    this.language.locale.locale = locale.value
    if (this.errors.has('locale')) this.errors.delete('locale')
  }

  handleChange = (input) => action((evt) => {
    this.language[input] = evt.target.value
    if (this.errors.has(input)) this.errors.delete(input)
  })

  render() {
    const { children } = this.props
    if (Children.count(children)) return cloneElement(children, { language: this.language })

    return (
      <div className="row-fluid">
        <div className="span6">

          <div className={ cx('control-group', {'error': this.errors.has('language')}) }>
            <label className="control-label" htmlFor="input-language">Мова:</label>
            <div className="controls">
              <Select
                id="input-language"
                disabled={this.isEdit}
                hideArrow={this.isEdit}
                value={this.language.language}
                options={this.languagesOptions}
                onChange={this.setLanguage}
                clearable={false}
              />

              {this.errors.has('language') &&
                <span className="help-inline">
                  <i className="zmdi zmdi-alert-triangle"></i> {this.errors.get('language')}
                </span>
              }
            </div>
          </div>

          <div className={ cx('control-group', {'error': this.errors.has('locale')}) }>
            <label className="control-label" htmlFor="input-locale">Локаль:</label>
            <div className="controls">
              <Select
                id="input-locale"
                disabled={this.isEdit}
                hideArrow={this.isEdit}
                name="form-field-name"
                value={this.language.locale.locale}
                options={this.localesOptions}
                onChange={this.setLocale}
              />

              {this.errors.has('locale') &&
                this.errors.get('locale').hasOwnProperty('locale') &&
                  <span className="help-inline">
                    <i className="zmdi zmdi-alert-triangle"></i> {this.errors.get('locale').locale}
                  </span>
              }
            </div>
          </div>

        </div>

        <div className="span6">

          <div className={ cx('control-group', {'error': this.errors.has('description')}) }>
            <label className="control-label" htmlFor="input-descr">Опис:</label>
            <div className="controls">
              <input type="text" id="input-descr" className="input-xlarge"
                onChange={this.handleChange('description')}
                defaultValue={this.language.description} />

              {this.errors.has('description') &&
                <span className="help-inline">
                  <i className="zmdi zmdi-alert-triangle"></i> {this.errors.get('description')}
                </span>
              }
            </div>
          </div>

          <div className="control-group">
            <label className="control-label" htmlFor="input-enabled">
              Включена:
            </label>
            <div className="controls">
              <select className="span3" id="input-enabled"
                defaultValue={this.language.enabled}
                onChange={this.handleChange('enabled')}
              >
                <option value="1">Так</option>
                <option value="0">Ні</option>
              </select>
            </div>
          </div>

        </div>
      </div>
    )
  }

  static propTypes = {
    language: PropTypes.object.isRequired,
    errors: PropTypes.object
  }

}

export default LanguageDetails
