import React, { cloneElement } from 'react'
import { computed, observable, action } from 'mobx'
import { observer, inject } from 'mobx-react'
import Menu from './menu'
import Item from './item'
import Delete from './item/delete'
import './styles.css'

@inject('store')
@observer
class Languages extends React.PureComponent {

  @observable isLoading = false

  @computed get languages() {
    return this.props.store.languages.all()
  }

  get paramId() {
    const { languageId } = this.props.params
    return languageId
  }

  @action enableLoader = (flag = true) => {
    this.isLoading = flag
  }

  render() {
    return (
      <div>
        {!this.isLoading && <Menu />}
        <table className="table table-hover table-bordered">
          {this.renderTHead()}
          {this.renderTBody()}
        </table>
      </div>
    )
  }

  renderTHead = () => (
    <thead>
      <tr>
        <th className="flag-icon-box">
          <i className="zmdi zmdi-flag"></i>
        </th>
        <th>Мова</th>
        <th>Локаль</th>
        <th>Опис</th>
        <th>Включено</th>
        <th>Дії</th>
      </tr>
    </thead>
  )

  renderTBody = () => {
    const { children } = this.props
    const paramId = this.paramId

    return (
      <tbody>
        {Object.values(this.languages).map((language) => {
          if (paramId === language.id) {
            return cloneElement(children, {
              language,
              key: language.id,
              enableLoader: this.enableLoader,
              isLoading: this.isLoading
            })
          }
          return (<Item language={language} key={language.id} isLoading={this.isLoading} />)
        })}
      </tbody>
    )
  }

  static Delete = Delete
}

export default Languages
