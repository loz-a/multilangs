import React from 'react';
import FlashMessages from 'app/components/common/flash-messages'
import './styles.css'

class App extends React.Component {
  render() {
    const { children } = this.props
    return (
      <div className="app-container">
        <FlashMessages />
        <div className="app">
          {children}
        </div>
      </div>
    )
  }
}

export default App
