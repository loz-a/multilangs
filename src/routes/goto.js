import config from '../config'
import history from '../history'

const dvPath = config.routes.multilangs.save.displayValues

function replaceParam (str, param) {
  const reOptional = /\(\/:(.*)\)$/gi
  const reRequired = /:(.*)$/
  if (param) {
    if (str.match(reOptional, str)) return str.replace(reOptional, `/${param}`)
    else return str.replace(reRequired, param)
  }
  return str.replace(reOptional, '') // if parameter is optional
}

function url(strings, ...values) {
  let step = ''
  let steps = ''
  let result = []
  let root = config.routes

  for (let i=0; i<values.length+1; i++) {
    steps = strings[i].split('/')
    for (let j=0; j<steps.length; j++) {
      step = steps[j]
      if (!step) continue
      if (!step in root) throw new Error('Invalid path in routes.url()')
      if ({}.toString.call(root[step]).slice(8, -1) === 'Object') {
        if (root[step].index) result.push(replaceParam(root[step].index, values[i]))
        root = root[step]
      } else {
        result.push(replaceParam(root[step], values[i]))
      }
    }
  }

  return result.join('/')
}

export const urls = {
  languages: () => url`multilangs`,
  languageSave: (languageId = null) => url`multilangs/save/${languageId}`,
  languageDelete: (languageId) => url`multilangs/delete/${languageId}`,
  displayValues: (languageId = null) => url`multilangs/save/${languageId}/displayValues`,
  displayValueAdd: (languageId = null) => url`multilangs/save/${languageId}/displayValues/add`,
  displayValueEdit: (languageId, displayValueId) => url`multilangs/save/${languageId}/displayValues/edit/${displayValueId}`,
  displayValueDelete: (languageId, displayValueId) => url`multilangs/save/${languageId}/displayValues/delete/${displayValueId}`
}

export default function goto(url, hystoryMethod = 'push') {
  if (typeof url !== 'string') throw new Error('Url must be string type')
  history[hystoryMethod](url)
}
