import React from 'react'
import { Router, Route, IndexRoute, Redirect } from 'react-router'
import history from '../history'

import config from '../config'
import PageNotFound from '../components/page-not-found'
import App from '../components/app'
import Languages from '../components/languages'

import SaveLanguage from '../components/languages/item/save'
import LanguageDetails from '../components/languages/item/save/details'
import DisplayValues from '../components/languages/item/save/display-values'

import breadcrumbs from 'app/decorators/breadcrumbs'

const routes = config.routes.multilangs

export default (
  <Router history={history}>

    <Route path={routes.index} component={breadcrumbs('Багатомовність.Список мов')(App)}>
      <IndexRoute component={Languages}/>

      <Route component={Languages}>
        <Route path={routes.delete} component={Languages.Delete} />
      </Route>

      <Route component={SaveLanguage}>
        <Route path={routes.save.index} component={breadcrumbs('Додати|Редагувати')(LanguageDetails)}>
          <Route path={routes.save.displayValues.index} component={breadcrumbs('Вивід мови')(DisplayValues)}>
            <Route path={routes.save.displayValues.add} component={breadcrumbs('Додати')(DisplayValues.Add)}/>
            <Route path={routes.save.displayValues.edit} component={breadcrumbs('Редагувати')(DisplayValues.Edit)}/>
            <Route path={routes.save.displayValues.delete} component={DisplayValues.Delete} />
          </Route>
        </Route>
      </Route>

    </Route>

    <Route path="*" component={PageNotFound} />
  </Router>
)
