import axios from 'axios'
import config from 'app/config'

const { url } = config

export default {
  saveLanguage(data) {
    return axios.post(url.languageSave, data)
  },

  deleteLangauge(languageId) {
    return axios.post(url.languageDelete, { languageId })
  }

}
