import React from 'react'

export default (title) => (DecoratedComponent) => {
  DecoratedComponent.breadcrumbsTitle = title
  return DecoratedComponent
}
