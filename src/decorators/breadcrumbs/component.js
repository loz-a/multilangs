import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import shortid from 'shortid'

const Divider = () => (
  <span className="divider">/</span>
)

const Breadcrumbs = ({
  items,
  homeIcon = 'zmdi zmdi-gps-dot'
}) => {

  const listItems = []

  if (items.length === 1) {
    listItems.push(<li key={shortid.generate()}><i className={homeIcon}></i> {items[0].title} </li>)
  }

  if (items.length > 1) {
    const homeItem = items.shift()
    const home = (
      <li key={shortid.generate()}>
        <i className={homeIcon}></i> <Link to={homeItem.path}>{homeItem.title}</Link>
        <Divider />
      </li>
    )
    const active = (<li key={shortid.generate()} className="active">{items.pop().title}</li>)

    listItems.push(
      home,
      ...items.map((item) => (
        <li key={shortid.generate()}>
          <Link to={item.path}>{item.title}</Link> <Divider/>
        </li>
      )),
      active
    )
  }

  return (
    <ul className="breadcrumb">
       { listItems }
     </ul>
  )

}

Breadcrumbs.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      path: PropTypes.string
    })
  ),
  homeIcon: PropTypes.string
}

export default Breadcrumbs
