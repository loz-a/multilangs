import React, { PropTypes } from 'react'
import { withRouter, formatPattern } from 'react-router'
import Breadcrumbs from './component'

export default (DecoratedComponent) => {

  const withBreadcrumbs = ({
    router,
    ...otherProps
  }) => {

      const { params, routes } = router

      const items = routes.reduce((acc, route) => {
        const { component } = route
        if (component && component.breadcrumbsTitle) {
          if (route.path) acc.pathAcc.push(route.path)
          acc.nodes.push({
            title: component.breadcrumbsTitle,
            path: formatPattern(acc.pathAcc.slice().join('/'), params)
          })
        }
        return acc
      }, {
        nodes: [],
        pathAcc: []
      }).nodes

      const renderBreadcrumbs = (items) => (homeIcon) => {        
        return (
          <Breadcrumbs items={items} homeIcon={homeIcon} />
        )
      }

      return <DecoratedComponent {...otherProps} renderBreadcrumbs={renderBreadcrumbs(items)} />
    }

    withBreadcrumbs.WrappedComponent = DecoratedComponent

    return withRouter(withBreadcrumbs)
}
