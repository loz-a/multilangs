export default {
  url: {
    languageSave: "/uk/editor/multilangs/save_language",
    languageDelete: "/uk/editor/multilangs/delete_language",
    toggleEnable: "/uk/editor/multilangs/toggle_enable_language"
  },
  routes: {
    multilangs: {
      index: "/uk/editor/multilangs",
      save: {
        index: "save(/:languageId)",
        displayValues: {
          index: "display-values",
          add: "add",
          edit: "edit/:displayValueId",
          delete: "delete/:displayValueId"
        }
      },
      delete: "delete/:languageId"
    }
  }
}
