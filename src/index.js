import React from 'react'
import { render } from 'react-dom'
import { useStrict } from 'mobx'
import { Provider } from 'mobx-react'
import routes from './routes'
import Store from './store'

import DevTools from 'mobx-react-devtools'

useStrict(true)

$(document).ready(() => {

  const store = new Store(window.INIT_STATE)

  if (DEVELOPMENT) {
    window.store = store
  }

  render(
    <div>
      <Provider store={store}>
        {routes}
      </Provider>
      <DevTools/>
    </div>,
    document.getElementById('multilangs')
  )

})
