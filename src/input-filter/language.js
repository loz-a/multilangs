import InputFilter from 'app/utils/input-filter'
import validators from './validators/languages'
import filters from './filters/language'

const postProcessor = (language) => {  
  const data = {}

  data['language'] = language['language']
  data['locale'] = language['locale']
  data['description'] = language['description'] || ''
  data['is_enable'] = language['enabled']

  data['display_values'] = Object
    .values(language['displayValues'] || [])
    .reduce((acc, item) => {
      acc.push({ ...item, 'display_value': item.displayValue })
      return acc
    }, [])

  if (language['id']) data['id'] = language['id']

  return data
}

export default new InputFilter(validators, filters, postProcessor)
