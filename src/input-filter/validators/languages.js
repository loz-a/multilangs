import InputFilter from 'app/utils/input-filter'
import localeValidators from './locale'
import localeFilters from '../filters/locale'

export default {
  __required: {
    language: true,
    locale: true
  },

  language: (value, context, store) => {
    const errors = []
    if (typeof value !== 'string') errors.push('Значення введене не вірно')
    if (!value.trim().length) errors.push('Значення не може бути пустим')

    if (!context.hasOwnProperty('id')) {
      if (store.languages.has(value)) errors.push('Запис з таким значенням вже існує')
    }

    return (errors.length) ? errors : true
  },

  locale: new InputFilter(localeValidators, localeFilters),

  description: (value) => {
    const errors = []
    if (typeof value !== 'string') errors.push('Значчення введене не вірно')
    if (value.length > 255) errors.push(`Довжина значення не повинна перевищувати 255 символів. Поточна довжина ${value.length}`)

    return errors.length ? errors : true
  }
}
