export default {
  __required: {    
    locale: true
  },

  locale: (value) => {
    const errors = []
    if (typeof value !== 'string') errors.push('Значчення введене не вірно')
    if (!value.trim().length) errors.push('Значення не може бути пустим')

    return (errors.length) ? errors : true
  }
}
