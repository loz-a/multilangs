import InputFilter from 'app/utils/input-filter'
import localeValidators from './locale'
import localeFilters from 'app/input-filter/filters/locale'

export default {
  __required: {
    displayValue: true
  },

  displayValue: (value) => {
    const errors = []
    if (typeof value !== 'string') errors.push('Значчення введене не вірно')
    if (value.length > 50) errors.push(`Довжина значення не повинна перевищувати 255 символів. Поточна довжина ${value.length}`)

    return errors.length ? errors : true
  }
}
