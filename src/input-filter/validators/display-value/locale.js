export default {
  __required: {
      locale: true
  },

  locale: (value, context = null, language) => {    
    const errors = []
    if (typeof value !== 'string') errors.push('Значчення введене не вірно')
    if (!value.trim().length) errors.push('Значення не може бути пустим')

    const isUnique = Object.values(language.displayValues).filter((item) => item.locale === value).length == 0
    if (!isUnique) errors.push('Запис з таким іменем локалі вже існує')

    return (errors.length) ? errors : true
  }
}
