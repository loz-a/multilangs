import InputFilter from 'app/utils/input-filter'
import validators from './validators/display-value/edit'
import filters from './filters/display-value'

export default new InputFilter(validators, filters)
