export default {
  language: (value) => {
    if (typeof value !== 'string') return value
    return value.trim()
  },

  description: (value) => {    
    if (typeof value !== 'string') return value
    return value.trim()
  }
}
