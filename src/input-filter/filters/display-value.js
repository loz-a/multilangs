export default {
  displayValue: (value) => {    
    if (typeof value !== 'string') return value
    return value.trim()
  }
}
