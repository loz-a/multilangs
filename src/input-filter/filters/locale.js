export default {
  locale: (value) => {
    if (typeof value !== 'string') return value
    return value.trim()
  }
}
